## HDRI

HDRI Haven by Greg Zaal:
High quality HDRIs for free
https://hdrihaven.com/

UnderWaterScene: http://www.blendswap.com/blends/view/78392 (license: CC-BY 3.0)

photo studio objects: http://www.blendswap.com/blends/view/50740 (license: CC-BY 3.0)

## Godot Projects

1) Godot demo projects by Juan Linietsky (reduz), Ariel Manzur (punto-) and contributors:
https://github.com/godotengine/godot-demo-projects
- GUI in 3D

2) Godot 3 Presentation by GDquest and contributors:
https://github.com/GDquest/godot-3-presentation (license: MIT / CC-BY 4.0)

License: MIT


## Software:

Godot Engine: https://godotengine.org/

Blender: https://www.blender.org/

Gimp: https://www.gimp.org/

MakeHuman: http://www.makehuman.org/

Inkscape: https://inkscape.org/


## Plugins
glTF-Blender-Exporter: https://github.com/KhronosGroup/glTF-Blender-Exporter

License: Apache License 2.0

## certs

ca-certificates: https://github.com/godotengine/godot/blob/master/thirdparty/certs/ca-certificates.crt

Third party library (see https://github.com/godotengine/godot/blob/master/thirdparty/README.md):

- Upstream: Mozilla, via https://apps.fedoraproject.org/packages/ca-certificates
- Version: 2018.2.22
- License: MPL 2.0

File extracted from a recent Fedora install:
/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
(It can't be extracted directly from the package,
as it's generated on the user's system.)

## License

CC0: https://creativecommons.org/publicdomain/zero/1.0/legalcode

CC-BY 3.0: https://creativecommons.org/licenses/by/3.0/legalcode

CC-BY 3.0: https://creativecommons.org/licenses/by/4.0/legalcode

CC BY-NC 4.0: https://creativecommons.org/licenses/by-nc/4.0/legalcode

MIT: https://opensource.org/licenses/mit-license.php

OFL: http://scripts.sil.org/OFL

Apache License 2.0: https://www.apache.org/licenses/LICENSE-2.0.html

MPL 2.0: https://www.mozilla.org/en-US/MPL/2.0/



