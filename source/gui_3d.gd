extends Spatial

# Member variables
var prev_pos = null
var last_click_pos = null
var viewport = null

var camrotx = 0.0
var perrotx = 0.0
var camroty = PI / 180
var zoom = 10
var time = 0
var anim = 0
var speed = 1
var next = false

var hdrs=[
	{ path="res://hdri/studio.hdr", name="Studio"},
	{ path="res://hdri/hill.hdr", name="Hill"},
	{ path="res://hdri/underwater.hdr", name="Underwater"},
]
var anims=[
	{ name="None"},
	{ name="Fade"},
	{ name="Turn"},
]



func _input(event):
	if (event is InputEventMouseMotion):
		if (event.button_mask&BUTTON_MASK_MIDDLE):
			camrotx -= event.relative.x*0.005
			camroty -= event.relative.y*0.005
			get_node("cambaseY").set_rotation(Vector3(0, camrotx + perrotx, 0))
			get_node("cambaseY/cambaseX").set_rotation(Vector3(camroty, PI, 0))
	if (event is InputEventMouseButton and event.button_index == BUTTON_WHEEL_UP and zoom > 9.5):
		zoom *= 0.95
		get_node("cambaseY/cambaseX/Camera").set_translation(Vector3(get_node("cambaseY/cambaseX/Camera").get_translation().x, get_node("cambaseY/cambaseX/Camera").get_translation().y, zoom))
	if (event is InputEventMouseButton and event.button_index == BUTTON_WHEEL_DOWN and zoom < 30):
		zoom *= 1.05
		get_node("cambaseY/cambaseX/Camera").set_translation(Vector3(get_node("cambaseY/cambaseX/Camera").get_translation().x, get_node("cambaseY/cambaseX/Camera").get_translation().y, zoom))
	if event.is_action_pressed("ui_right"):
		next = true
		animation()
	if event.is_action_pressed("ui_left"):
		next = false
		animation()
	# Check if the event is a non-mouse event
	var is_mouse_event = false
	var mouse_events = [InputEventMouseMotion, InputEventScreenDrag, InputEventScreenTouch]
	for mouse_event in mouse_events:
		if event is mouse_event:
			is_mouse_event = true
			break
	if event is InputEventMouseButton:
		if not (event.button_index == BUTTON_WHEEL_UP or event.button_index == BUTTON_WHEEL_DOWN):
			is_mouse_event = true
  
	# If it is, then pass the event to the viewport
	if is_mouse_event == false:
		viewport.input(event)

func animation():
	time = 0
	set_process(true)

func _process(delta):
	if anim == 0:
		next_slide()
		set_process(false)
	elif anim == 1:
		var mat = $Area/Quad.get_surface_material(0)
		if time < 1:
			mat.albedo_color = Color(1,1,1,1-time)
		elif time < 2:
			next_slide()
			time += 1
			mat.albedo_color = Color(1,1,1,0)
		elif time < 3:
			mat.albedo_color = Color(1,1,1,time-2)
		else:
			mat.albedo_color = Color(1,1,1,1)
			set_process(false)
	elif anim == 2:
		if time < 1:
			$Area.rotation.y = PI/2*time
		elif time < 2:
			time += 1
			next_slide()
			$Area.rotation.y = PI/2*(time-3)
		elif time < 3:
			$Area.rotation.y = PI/2*(time-3)
		else:
			$Area.rotation.y = 0
			set_process(false)
		if next:
			$Area.rotation.y *= -1
	time += delta*speed

func next_slide():
	if next == true:
		$Viewport/Presentation.go_to_next_slide($Viewport/Presentation.NEXT)
	else:
		$Viewport/Presentation.go_to_next_slide($Viewport/Presentation.PREVIOUS)

func apply_texture():
	var img = get_node("Viewport").get_viewport().get_texture().get_data()
	var tex = ImageTexture.new()
	tex.create_from_image(img)
	tex.flags = tex.FLAG_VIDEO_SURFACE
	var mat = get_node("Quad2").get_surface_material(0)
	mat.flags_unshaded=true
	mat.flags_transparent=true
	mat.albedo_texture = tex
	#get_node("Quad2").show()
# Mouse events for Area
func _on_area_input_event(camera, event, click_pos, click_normal, shape_idx):
	# Use click pos (click in 3d space, convert to area space)
	var pos = get_node("Area").get_global_transform().affine_inverse()
	# the click pos is not zero, then use it to convert from 3D space to area space
	if click_pos.x != 0 or click_pos.y != 0 or click_pos.z != 0:
		pos *= click_pos
		last_click_pos = click_pos
	else:
		# Otherwise, we have a motion event and need to use our last click pos
		# and move it according to the relative position of the event.
		# NOTE: this is not an exact 1-1 conversion, but it's pretty close
		pos *= last_click_pos
		if event is InputEventMouseMotion or event is InputEventScreenDrag:
			pos.x += event.relative.x / viewport.size.x
			pos.y += event.relative.y / viewport.size.y
			last_click_pos = pos
  
	# Convert to 2D
	pos = Vector2(pos.x, pos.y)
	# Convert to viewport coordinate system
	pos.x = (pos.x + 19.2/2)*100
	pos.y = (-pos.y + 10.8/2)*100
	
	# Set the position in event
	event.position = pos
	event.global_position = pos
	if not prev_pos:
		prev_pos = pos
	if event is InputEventMouseMotion:
		event.relative = pos - prev_pos
	prev_pos = pos
	
	# Send the event to the viewport
	viewport.input(event)


func _ready():
	for h in hdrs:	
		get_node("bg").add_item(h.name)
	for a in anims:	
		get_node("anim").add_item(a.name)
	set_process(false)
	get_node("cambaseY/cambaseX/Camera").set_translation(Vector3(get_node("cambaseY/cambaseX/Camera").get_translation().x, get_node("cambaseY/cambaseX/Camera").get_translation().y, zoom))
	viewport = get_node("Viewport")
	get_node("Area").connect("input_event", self, "_on_area_input_event")
  


func _on_bg_item_selected(ID):
	get_node("environment").environment.background_sky.panorama = load(hdrs[ID].path)


func _on_anim_item_selected(ID):
	anim = ID
